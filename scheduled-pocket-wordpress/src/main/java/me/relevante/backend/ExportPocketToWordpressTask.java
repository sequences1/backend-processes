package me.relevante.backend;

import me.relevante.backend.service.ExportPocketToWordpressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ExportPocketToWordpressTask {

    @Autowired private ExportPocketToWordpressService exportService;

    @Scheduled(fixedRateString = "${custom.schedule.pocketWordpress.activationScheduleInMilliseconds}")
    public void exportPocketToWordpress() throws Exception {
        exportService.exportPocketToWordpress();
    }

}