package me.relevante.backend.service;

import me.relevante.api.PocketApiClient;
import me.relevante.api.PocketApiClientImpl;
import me.relevante.api.WordpressApiClient;
import me.relevante.api.WordpressApiClientImpl;
import me.relevante.api.WordpressApiException;
import me.relevante.auth.NetworkBasicAuthCredentials;
import me.relevante.auth.NetworkOAuthCredentials;
import me.relevante.backend.model.PocketWordpressItem;
import me.relevante.backend.persistence.PocketItemRepo;
import me.relevante.backend.persistence.PocketWordpressItemRepo;
import me.relevante.backend.persistence.PocketWordpressMappingRepo;
import me.relevante.backend.persistence.RelevanteAccountRepo;
import me.relevante.backend.persistence.WordpressHubRepo;
import me.relevante.core.Pocket;
import me.relevante.core.PocketItem;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.Wordpress;
import me.relevante.core.WordpressCategory;
import me.relevante.core.WordpressFormat;
import me.relevante.core.WordpressHub;
import me.relevante.core.WordpressMedia;
import me.relevante.core.WordpressPost;
import me.relevante.core.WordpressProfile;
import me.relevante.core.WordpressStatus;
import me.relevante.hub.PocketWordpressMapping;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class ExportPocketToWordpressService {

    private static final Logger logger = LoggerFactory.getLogger(ExportPocketToWordpressService.class);
    private static final String POCKET_ALL_ITEMS_TAG = "*";

    private final PocketWordpressMappingRepo pocketWordpressMappingRepo;
    private final RelevanteAccountRepo relevanteAccountRepo;
    private final PocketWordpressItemRepo pocketWordpressItemRepo;
    private final WordpressHubRepo wordpressHubRepo;
    private final PocketItemRepo pocketItemRepo;

    @Autowired
    public ExportPocketToWordpressService(final PocketWordpressMappingRepo pocketWordpressMappingRepo,
                                          final RelevanteAccountRepo relevanteAccountRepo,
                                          final PocketWordpressItemRepo pocketWordpressItemRepo,
                                          final WordpressHubRepo wordpressHubRepo,
                                          final PocketItemRepo pocketItemRepo) {
        this.pocketWordpressMappingRepo = pocketWordpressMappingRepo;
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.pocketWordpressItemRepo = pocketWordpressItemRepo;
        this.wordpressHubRepo = wordpressHubRepo;
        this.pocketItemRepo = pocketItemRepo;
    }

    public void exportPocketToWordpress() {

        final List<PocketWordpressMapping> pocketWordpressMappings = getAllMappingsOnePerHub();
        for (final PocketWordpressMapping pocketWordpressMapping : pocketWordpressMappings) {
            final RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(pocketWordpressMapping.getRelevanteId());
            if (relevanteAccount == null)
                continue;
            final NetworkOAuthCredentials<Pocket> pocketCredentials = (NetworkOAuthCredentials<Pocket>)
                    relevanteAccount.getCredentials(Pocket.getInstance());
            final NetworkBasicAuthCredentials<Wordpress> wpCredentials = (NetworkBasicAuthCredentials<Wordpress>)
                    relevanteAccount.getCredentials(Wordpress.getInstance());
            exportPocketToWordpressMapping(pocketWordpressMapping, pocketCredentials, wpCredentials);
        }

    }

    private List<PocketWordpressMapping> getAllMappingsOnePerHub() {
        final List<PocketWordpressMapping> allMappings = pocketWordpressMappingRepo.findAll();
        final Map<String, List<PocketWordpressMapping>> pocketWordpressMappingsByHubId = new HashMap<>();
        for (final PocketWordpressMapping pocketWordpressMapping : allMappings) {
            List<PocketWordpressMapping> pocketWordpressMappings = pocketWordpressMappingsByHubId.get(pocketWordpressMapping.getWpHubId());
            if (pocketWordpressMappings == null) {
                pocketWordpressMappings = new ArrayList<>();
                pocketWordpressMappingsByHubId.put(pocketWordpressMapping.getWpHubId(), pocketWordpressMappings);
            }
            pocketWordpressMappings.add(pocketWordpressMapping);
        }
        final List<PocketWordpressMapping> mappingsOnePerHub = new ArrayList<>();
        while (!pocketWordpressMappingsByHubId.isEmpty()) {
            final List<String> hubIds = new ArrayList<>(pocketWordpressMappingsByHubId.keySet());
            for (final String hubId : hubIds) {
                final List<PocketWordpressMapping> hubMappings = pocketWordpressMappingsByHubId.get(hubId);
                mappingsOnePerHub.add(hubMappings.remove(0));
                if (hubMappings.isEmpty()) {
                    pocketWordpressMappingsByHubId.remove(hubId);
                }
            }
        }
        return mappingsOnePerHub;
    }

    private void exportPocketToWordpressMapping(final PocketWordpressMapping mapping,
                                                final NetworkOAuthCredentials<Pocket> pocketCredentials,
                                                final NetworkBasicAuthCredentials<Wordpress> wpCredentials) {
        final PocketApiClient pocketApiClient = new PocketApiClientImpl(pocketCredentials.getOAuthConsumerKeyPair(), pocketCredentials.getOAuthAccessTokenPair());
        final String pocketTag = mapping.getPocketTag();
        final List<PocketItem> pocketItems = (pocketTag.equals(POCKET_ALL_ITEMS_TAG)) ? pocketApiClient.getItems() : pocketApiClient.getItemsByTag(pocketTag);
        pocketItems.sort((o1, o2) -> Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId())));
        for (final PocketItem pocketItem : pocketItems) {
            exportPocketItem(pocketItem, mapping, wpCredentials);
        }

    }

    private void exportPocketItem(final PocketItem pocketItem,
                                  final PocketWordpressMapping mapping,
                                  final NetworkBasicAuthCredentials<Wordpress> wpCredentials) {
        if (!pocketItemRepo.exists(pocketItem.getId())) {
            pocketItemRepo.save(pocketItem);
        }
        final PocketWordpressItem existingPocketWordpressItem = pocketWordpressItemRepo.findOneByPocketItemIdAndWordpressHubId(pocketItem.getId(), mapping.getWpHubId());
        if (existingPocketWordpressItem != null) {
            if (existingPocketWordpressItem.getCreationTimestamp() == null) {
                final Date date = Date.from(LocalDateTime.now().minusWeeks(2).toInstant(ZoneOffset.UTC));
                existingPocketWordpressItem.setCreationTimestamp(date);
                pocketWordpressItemRepo.save(existingPocketWordpressItem);
            }
            return;
        }
        waitSomeTime();
        exportPocketItemToWordpress(pocketItem, mapping.getWpHubId(), mapping.getWpCategory(), mapping.getLanguage(), wpCredentials);
        final PocketWordpressItem newPocketWordpressItem = new PocketWordpressItem(pocketItem.getId(), mapping.getWpHubId(), new Date());
        pocketWordpressItemRepo.save(newPocketWordpressItem);
        pocketItemRepo.save(pocketItem);
    }

    private void exportPocketItemToWordpress(final PocketItem pocketItem,
                                             final String wpHubId,
                                             final String wpCategoryName,
                                             final String language,
                                             final NetworkBasicAuthCredentials<Wordpress> wpCredentials) {
        final WordpressHub wpHub = wordpressHubRepo.findOne(wpHubId);
        final WordpressApiClient wpApiClient = new WordpressApiClientImpl(wpHub.getUrl(), wpCredentials.getUsername(), wpCredentials.getPassword());
        final WordpressCategory wpCategory = obtainWpCategory(wpApiClient, wpCategoryName);
        final WordpressProfile wpProfile = wpApiClient.getProfile();
        final WordpressPost wpPost = createWordpressPostFromPocketItem(pocketItem, wpCategory, wpProfile.getLocalId());
        if (pocketItem.hasImage()) {
            try {
                WordpressMedia wpImage = wpApiClient.uploadImage(pocketItem.getImages().get(0).getSrc());
                wpPost.setFeaturedMediaId(wpImage.getId());
            } catch (WordpressApiException e) {
                logger.error("Error", e);
            }
        }
        wpApiClient.createPostByLanguage(wpPost, language);
    }

    private WordpressCategory obtainWpCategory(final WordpressApiClient wpApiClient,
                                               final String categoryName) {
        final List<WordpressCategory> categories = wpApiClient.getCategories();
        for (final WordpressCategory category : categories) {
            if (category.getName().equals(categoryName)) {
                return category;
            }
        }
        return null;
    }

    private WordpressPost createWordpressPostFromPocketItem(final PocketItem pocketItem,
                                                            final WordpressCategory wpCategory,
                                                            final Long wpUserId) {
        WordpressPost wpPost = new WordpressPost();
        wpPost.setAuthor(wpUserId);
        wpPost.setTitle(StringUtils.isBlank(pocketItem.getGivenTitle()) ? pocketItem.getResolvedTitle() : pocketItem.getGivenTitle());
        wpPost.setExcerpt(pocketItem.getExcerpt());
        wpPost.setFormat(WordpressFormat.STANDARD);
        wpPost.setContent(pocketItem.getResolvedUrl());
        Date date = (pocketItem.getCreationTimestamp() != null ? pocketItem.getCreationTimestamp() : new Date());
        LocalDateTime localDateTimeGmt = LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);
        wpPost.setDate(localDateTimeGmt);
        wpPost.setDateGmt(localDateTimeGmt);
        wpPost.setExcerpt(pocketItem.getExcerpt());
        if (wpCategory != null) {
            wpPost.getCategoryIds().add(wpCategory.getId());
        }
        wpPost.setOpenedToComments(true);
        wpPost.setOpenedToPing(true);
        wpPost.setSticky(false);
        wpPost.setStatus(WordpressStatus.PUBLISH);
        return wpPost;
    }

    private void waitSomeTime() {
        try {
            final long timeToWait = 10 + Math.round(Math.floor(Math.random()*10));
            TimeUnit.SECONDS.sleep(timeToWait);
        } catch(final InterruptedException e) {
            logger.error("Error waiting time", e);
        }
    }

}