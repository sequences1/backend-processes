package me.relevante.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.relevante.backend.queue.errorHandler.CustomErrorHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

import java.util.Properties;

@Configuration
public class BaseConsumerConfig implements RabbitListenerConfigurer {

	@Value("${spring.rabbitmq.concurrentConsumers}")
	protected Integer concurrentConsumers;
	@Value("${spring.rabbitmq.maxConcurrentConsumers}")
	protected Integer maxConcurrentConsumers;

    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }

    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(consumerJackson2MessageConverter());
        return factory;
    }

    @Override
    public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
    }

	@Bean(name = "nlpResourcesPath")
	public String getNlpResourcesPath(@Value("${nlp.resources.path}") final String nlpResourcesPath) {
		return nlpResourcesPath;
	}

	@Bean
	public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(final CustomErrorHandler customErrorHandler,
																			   final ConnectionFactory connectionFactory) {
		final SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory);
		factory.setConcurrentConsumers(concurrentConsumers);
		factory.setMaxConcurrentConsumers(maxConcurrentConsumers);
		factory.setErrorHandler(customErrorHandler);
		return factory;

	}

	@Bean
	public com.rabbitmq.client.ConnectionFactory rabbitMQConnectionFactory(@Value("${spring.rabbitmq.host}") final String rabbitHost,
																		   @Value("${spring.rabbitmq.port}") final int rabbitPort,
																		   @Value("${spring.rabbitmq.username}") final String rabbitUsername,
																		   @Value("${spring.rabbitmq.password}") final String rabbitPassword,
																		   @Value("${spring.rabbitmq.virtualHost}") final String rabbitVirtualHost) {
		if (rabbitPort == 0) {
			return null;
		}
		final com.rabbitmq.client.ConnectionFactory factory = new com.rabbitmq.client.ConnectionFactory();
		factory.setHost(rabbitHost);
		factory.setPort(rabbitPort);
		factory.setUsername(rabbitUsername);
		factory.setPassword(rabbitPassword);
		factory.setVirtualHost(rabbitVirtualHost);
		return factory;
	}

	@Bean
	public JavaMailSender emailSender(@Value("${spring.email.host}") final String host,
									  @Value("${spring.email.port}") final int port,
									  @Value("${spring.email.username}") final String username,
									  @Value("${spring.email.password}") final String password,
									  @Value("${spring.email.useAuth}") final Boolean userAuth,
									  @Value("${spring.email.useTtls}") final Boolean useTtls) {
		final JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
		javaMailSenderImpl.setHost(host);
		javaMailSenderImpl.setPort(port);
		javaMailSenderImpl.setUsername(username);
		javaMailSenderImpl.setPassword(password);
		final Properties javaMailProperties = new Properties();
		javaMailProperties.setProperty("mail.smtp.auth", String.valueOf(userAuth));
		javaMailProperties.setProperty("mail.smtp.starttls.enable", String.valueOf(useTtls));
		javaMailSenderImpl.setJavaMailProperties(javaMailProperties);
		return javaMailSenderImpl;
	}

	@Bean
	public ObjectMapper jsonMapper() {
		return new ObjectMapper();
	}
}