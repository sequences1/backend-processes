package me.relevante.service.impl;

import me.relevante.api.TwitterApiClient;
import me.relevante.api.TwitterApiException;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.TwitterPost;
import me.relevante.ingest.TwitterIngestPost;
import me.relevante.persistence.BaseTwitterFullProfileRepo;
import me.relevante.request.TwitterExtractProfileActivityRequest;
import me.relevante.service.NetworkBulkRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TwitterExtractProfileActivityRequestService
        extends AbstractTwitterApiBulkRequestService<TwitterExtractProfileActivityRequest>
        implements NetworkBulkRequestService<Twitter, TwitterExtractProfileActivityRequest> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwitterExtractProfileActivityRequestService.class);

    private final TwitterIngestProcessor ingestProcessor;
    private final TwitterIngestConverter ingestConverter;
    private final BaseTwitterFullProfileRepo fullProfileRepo;
    private final TwitterLastPostAssigner lastPostAssigner;

    @Autowired
    public TwitterExtractProfileActivityRequestService(final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                       final CrudRepository<TwitterExtractProfileActivityRequest, String> extractProfileActivityRequestRepo,
                                                       final CrudRepository<RelevanteAccount, String> relevanteAccountRepo,
                                                       final TwitterIngestProcessor ingestProcessor,
                                                       final TwitterIngestConverter ingestConverter,
                                                       final BaseTwitterFullProfileRepo fullProfileRepo,
                                                       final TwitterLastPostAssigner lastPostAssigner) {
        super(extractProfileActivityRequestRepo, relevanteContextRepo, relevanteAccountRepo);
        this.ingestProcessor = ingestProcessor;
        this.ingestConverter = ingestConverter;
        this.fullProfileRepo = fullProfileRepo;
        this.lastPostAssigner = lastPostAssigner;
    }

    @Override
    public Class<TwitterExtractProfileActivityRequest> getActionRequestClass() {
        return TwitterExtractProfileActivityRequest.class;
    }

    @Override
    protected void execute(final TwitterExtractProfileActivityRequest actionRequest) {
        super.execute(actionRequest);
        actionRequest.getExtractedPosts().forEach(post -> ingestProcessor.processIngestPost(post));
        final TwitterFullProfile fullProfile = fullProfileRepo.findOne(actionRequest.getTargetProfileId());
        fullProfile.setActivityUpdated();
        fullProfileRepo.save(fullProfile);
        lastPostAssigner.assignLastPost(actionRequest.getTargetProfileId(), actionRequest.getRequesterRelevanteId());
    }

    @Override
    protected TwitterApiResponse executeApiAction(final TwitterExtractProfileActivityRequest extractProfileActivityRequest,
                                                  final TwitterApiClient apiClient) {
        TwitterApiResponse twitterApiResponse;
        try {
            final List<TwitterIngestPost> extractedProfilePosts = extractProfilePosts(extractProfileActivityRequest, apiClient);
            extractProfileActivityRequest.setExtractedPosts(extractedProfilePosts);
            extractProfileActivityRequest.setSuccess();
            actionRequestRepo.save(extractProfileActivityRequest);
            twitterApiResponse = new TwitterApiResponse(TwitterApiResult.SUCCESS, extractedProfilePosts);
        } catch (final TwitterApiException e) {
            LOGGER.error("Error", e);
            extractProfileActivityRequest.setError();
            twitterApiResponse = new TwitterApiResponse(TwitterApiResult.UNDEFINED_ERROR, e);
        }
        return twitterApiResponse;

    }

    private List<TwitterIngestPost> extractProfilePosts(final TwitterExtractProfileActivityRequest extractProfileActivityRequest,
                                                        final TwitterApiClient apiClient) {
        final TwitterFullProfile fullProfile = fullProfileRepo.findOne(extractProfileActivityRequest.getTargetProfileId());
        final List<TwitterPost> posts = apiClient.getTimeline(fullProfile.getProfile().getId());
        return posts.stream().map(post -> ingestConverter.createIngestPostFromPost(post)).collect(Collectors.toList());
    }
}
