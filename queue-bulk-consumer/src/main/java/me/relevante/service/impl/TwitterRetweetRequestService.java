package me.relevante.service.impl;

import me.relevante.api.TwitterApiClient;
import me.relevante.api.TwitterApiException;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.TwitterRetweet;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.TwitterEngageStatus;
import me.relevante.request.TwitterRetweetRequest;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TwitterRetweetRequestService
        extends AbstractTwitterEngageBulkRequestService<TwitterRetweetRequest, TwitterRetweet>
        implements NetworkBulkRequestService<Twitter, TwitterRetweetRequest> {

    @Autowired
    public TwitterRetweetRequestService(final CrudRepository<TwitterRetweetRequest, String> retweetRequestRepo,
                                        final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                        final CrudRepository<TwitterRetweet, String> retweetRepo,
                                        final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                        final CrudRepository<TwitterFullProfile, String> fullProfileRepo,
                                        final RelevanteProfileContextService relevanteProfileContextService,
                                        final CrudRepository<RelevanteAccount, String> relevanteAccountRepo) {
        super(retweetRequestRepo, relevanteContextRepo, retweetRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService,
                relevanteAccountRepo);
    }

    @Override
    public Class<TwitterRetweetRequest> getActionRequestClass() {
        return TwitterRetweetRequest.class;
    }

    @Override
    protected TwitterRetweet createActionFromActionRequest(final TwitterRetweetRequest retweetRequest) {
        return new TwitterRetweet(retweetRequest.getRequesterRelevanteId(), retweetRequest.getTargetProfileId(), retweetRequest.getTargetPostId());
    }

    @Override
    protected TwitterApiResponse executeApiAction(final TwitterRetweetRequest actionRequest, final TwitterApiClient apiClient) {
        try {
            return apiClient.retweet(actionRequest.getTargetPostId());
        } catch (final TwitterApiException e) {
            return new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
    }

    @Override
    protected void updateActionEngageStatus(final TwitterEngageStatus engageStatus, final ActionEngageStatus actionEngageStatus) {
        engageStatus.setRetweetStatus(actionEngageStatus);
    }
}
