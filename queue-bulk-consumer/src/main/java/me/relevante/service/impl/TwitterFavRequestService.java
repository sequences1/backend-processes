package me.relevante.service.impl;

import me.relevante.api.TwitterApiClient;
import me.relevante.api.TwitterApiException;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFav;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.TwitterEngageStatus;
import me.relevante.request.TwitterFavRequest;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TwitterFavRequestService
        extends AbstractTwitterEngageBulkRequestService<TwitterFavRequest, TwitterFav>
        implements NetworkBulkRequestService<Twitter, TwitterFavRequest> {

    @Autowired
    public TwitterFavRequestService(final CrudRepository<TwitterFavRequest, String> favRequestRepo,
                                    final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                    final CrudRepository<TwitterFav, String> favRepo,
                                    final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                    final CrudRepository<TwitterFullProfile, String> fullProfileRepo,
                                    final RelevanteProfileContextService relevanteProfileContextService,
                                    final CrudRepository<RelevanteAccount, String> relevanteAccountRepo) {
        super(favRequestRepo, relevanteContextRepo, favRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService, relevanteAccountRepo);
    }

    @Override
    public Class<TwitterFavRequest> getActionRequestClass() {
        return TwitterFavRequest.class;
    }

    @Override
    protected TwitterFav createActionFromActionRequest(final TwitterFavRequest favRequest) {
        return new TwitterFav(favRequest.getRequesterRelevanteId(), favRequest.getTargetProfileId(), favRequest.getTargetPostId());
    }

    @Override
    protected TwitterApiResponse executeApiAction(final TwitterFavRequest actionRequest, final TwitterApiClient apiClient) {
        try {
            return apiClient.favTweet(actionRequest.getTargetPostId());
        } catch (final TwitterApiException e) {
            return new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
    }

    @Override
    protected void updateActionEngageStatus(final TwitterEngageStatus engageStatus, final ActionEngageStatus actionEngageStatus) {
        engageStatus.setFavStatus(actionEngageStatus);
    }
}
