package me.relevante.service.impl;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullPost;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinGroup;
import me.relevante.core.LinkedinPost;
import me.relevante.core.RelevanteContext;
import me.relevante.ingest.LinkedinIngestGroupPost;
import me.relevante.ingest.LinkedinIngestShare;
import me.relevante.persistence.BaseLinkedinFullProfileRepo;
import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.request.LinkedinExtractProfileActivityRequest;
import me.relevante.service.NetworkBulkRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LinkedinExtractProfileActivityRequestService
        extends AbstractNetworkBulkRequestService<Linkedin, LinkedinExtractProfileActivityRequest>
        implements NetworkBulkRequestService<Linkedin, LinkedinExtractProfileActivityRequest> {

    private final BaseLinkedinFullProfileRepo fullProfileRepo;
    private final NetworkFullPostRepo<LinkedinFullPost> fullPostRepo;
    private final CrudRepository<LinkedinGroup, String> groupRepo;
    private final LinkedinLastPostAssigner lastPostAssigner;

    @Autowired
    public LinkedinExtractProfileActivityRequestService(final CrudRepository<LinkedinExtractProfileActivityRequest, String> actionRequestRepo,
                                                        final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                        final BaseLinkedinFullProfileRepo fullProfileRepo,
                                                        final NetworkFullPostRepo<LinkedinFullPost> fullPostRepo,
                                                        final CrudRepository<LinkedinGroup, String> groupRepo,
                                                        final LinkedinLastPostAssigner lastPostAssigner) {
        super(actionRequestRepo, relevanteContextRepo);
        this.fullProfileRepo = fullProfileRepo;
        this.fullPostRepo = fullPostRepo;
        this.groupRepo = groupRepo;
        this.lastPostAssigner = lastPostAssigner;
    }

    @Override
    public Class<LinkedinExtractProfileActivityRequest> getActionRequestClass() {
        return LinkedinExtractProfileActivityRequest.class;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    protected void execute(final LinkedinExtractProfileActivityRequest extractProfileActivityRequest) {
        if (extractProfileActivityRequest.isNotSuccessful()) {
            return;
        }
        if (extractProfileActivityRequest.getExtractedShares() == null && extractProfileActivityRequest.getExtractedGroupPosts() == null) {
            extractProfileActivityRequest.setError();
            return;
        }
        final LinkedinFullProfile existingFullProfile = fullProfileRepo.findOne(extractProfileActivityRequest.getTargetProfileId());
        for (final LinkedinIngestShare share : extractProfileActivityRequest.getExtractedShares()) {
            share.setAuthorUrl(existingFullProfile.getProfile().getProfileUrl());
            processIngestShare(share, existingFullProfile.getId());
        }
        for (final LinkedinIngestGroupPost groupPost : extractProfileActivityRequest.getExtractedGroupPosts()) {
            groupPost.setAuthorUrl(existingFullProfile.getProfile().getProfileUrl());
            processIngestGroupPost(groupPost);
        }
        existingFullProfile.setActivityUpdated();
        fullProfileRepo.save(existingFullProfile);
        lastPostAssigner.assignLastPost(existingFullProfile.getId(), extractProfileActivityRequest.getRequesterRelevanteId());
    }

    private void processIngestShare(final LinkedinIngestShare ingestShare,
                                    final String authorProfileId) {
        if (ingestShare == null) {
            return;
        }
        final LinkedinFullPost fullPostFromIngestShare = createFullPostFromIngestShare(ingestShare, authorProfileId);
        final LinkedinFullPost existingFullPost = fullPostRepo.findOne(fullPostFromIngestShare.getPost().getId());
        if (existingFullPost == null) {
            fullPostRepo.save(fullPostFromIngestShare);
        } else {
            existingFullPost.updateWithExistingDataIn(fullPostFromIngestShare);
            fullPostRepo.save(existingFullPost);
        }
    }

    private void processIngestGroupPost(final LinkedinIngestGroupPost ingestGroupPost) {
        if (ingestGroupPost == null) {
            return;
        }
        final LinkedinFullPost fullPostFromIngestGroupPost = createFullPostFromIngestGroupPost(ingestGroupPost);
        if (fullPostFromIngestGroupPost != null) {
            return;
        }
        final LinkedinFullPost fullPostFromIngestShare = createFullPostFromIngestGroupPost(ingestGroupPost);
        final LinkedinFullPost existingFullPost = fullPostRepo.findOne(fullPostFromIngestShare.getPost().getId());
        if (existingFullPost == null) {
            fullPostRepo.save(fullPostFromIngestShare);
        } else {
            existingFullPost.updateWithExistingDataIn(fullPostFromIngestShare);
            fullPostRepo.save(existingFullPost);
        }
    }

    private LinkedinFullPost createFullPostFromIngestShare(final LinkedinIngestShare ingestShare,
                                                           final String authorProfileId) {
        final LinkedinPost postFromIngestShare = createPostFromIngestShare(ingestShare, authorProfileId);
        final LinkedinFullPost fullPostFromIngestShare = new LinkedinFullPost(postFromIngestShare);
        return fullPostFromIngestShare;
    }

    private LinkedinPost createPostFromIngestShare(final LinkedinIngestShare ingestShare,
                                                   final String authorProfileId) {
        final LinkedinPost post = new LinkedinPost();
        final int idStartIndex = ingestShare.getContentUrl().lastIndexOf(":");
        post.setLinkedinId(ingestShare.getContentUrl().substring(idStartIndex + 1));
        post.setAuthorId(authorProfileId);
        post.setContent(ingestShare.getContentText());
        post.setCreationTimestamp(ingestShare.getCreationTimestamp());
        post.setImageUrl(ingestShare.getImageUrl());
        post.setTitle(ingestShare.getTitle());
        post.setSubTitle(ingestShare.getSubtitle());
        post.setUrl(ingestShare.getContentUrl().replace("https:", "http:"));
        return post;
    }

    private LinkedinFullPost createFullPostFromIngestGroupPost(final LinkedinIngestGroupPost ingestGroupPost) {
        final LinkedinPost postFromIngestGroupPost = createPostFromIngestGroupPost(ingestGroupPost);
        if (postFromIngestGroupPost == null) {
            return null;
        }
        final LinkedinFullPost fullPostFromIngestGroupPost = new LinkedinFullPost(postFromIngestGroupPost);
        return fullPostFromIngestGroupPost;
    }

    private LinkedinPost createPostFromIngestGroupPost(final LinkedinIngestGroupPost ingestGroupPost) {
        final LinkedinPost post = new LinkedinPost();
        final LinkedinFullProfile authorFullProfile = fullProfileRepo.findOneByProfileProfileUrl(ingestGroupPost.getAuthorUrl());
        if (authorFullProfile == null) {
            return null;
        }
        final LinkedinGroup group = groupRepo.findOne(ingestGroupPost.getGroupId());
        if (group == null) {
            return null;
        }
        final int idStartIndex = ingestGroupPost.getContentUrl().lastIndexOf("/");
        post.setLinkedinId(ingestGroupPost.getContentUrl().substring(idStartIndex + 1));
        post.setGroupId(ingestGroupPost.getGroupId());
        post.setAuthorId(authorFullProfile.getId());
        post.setContent(ingestGroupPost.getContentText());
        post.setCreationTimestamp(new Date());
        post.setImageUrl(ingestGroupPost.getImageUrl());
        post.setTitle(ingestGroupPost.getTitle());
        post.setSubTitle(ingestGroupPost.getSubtitle());
        post.setUrl(ingestGroupPost.getContentUrl());
        return post;
    }

}
