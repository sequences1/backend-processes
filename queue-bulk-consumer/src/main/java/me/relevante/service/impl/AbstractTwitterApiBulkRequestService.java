package me.relevante.service.impl;

import me.relevante.api.TwitterApiClient;
import me.relevante.api.TwitterApiClientImpl;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.auth.NetworkOAuthCredentials;
import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Twitter;
import me.relevante.request.NetworkActionRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;

import java.util.concurrent.TimeUnit;

public abstract class AbstractTwitterApiBulkRequestService<R extends NetworkActionRequest<Twitter>>
        extends AbstractNetworkBulkRequestService<Twitter, R>
        implements NetworkBulkRequestService<Twitter, R> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTwitterApiBulkRequestService.class);

    private final CrudRepository<RelevanteAccount, String> relevanteAccountRepo;

    public AbstractTwitterApiBulkRequestService(final CrudRepository<R, String> actionRequestRepo,
                                                final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                final CrudRepository<RelevanteAccount, String> relevanteAccountRepo) {
        super(actionRequestRepo, relevanteContextRepo);
        this.relevanteAccountRepo = relevanteAccountRepo;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    protected void execute(final R actionRequest) {
        if (actionRequest.isNotInProgress()) {
            return;
        }
        final TwitterApiClient apiClient = getTwitterApiClient(actionRequest.getRequesterRelevanteId());
        waitSomeTime();
        final TwitterApiResponse apiResponse = executeApiAction(actionRequest, apiClient);
        updateActionRequestWithApiResult(actionRequest, apiResponse.getResult());
    }

    protected abstract TwitterApiResponse executeApiAction(final R actionRequest, final TwitterApiClient apiClient);

    private TwitterApiClient getTwitterApiClient(final String relevanteId) {
        final RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        final NetworkOAuthCredentials<Twitter> credentials = (NetworkOAuthCredentials<Twitter>) relevanteAccount.getCredentials(
                Twitter.getInstance());
        return new TwitterApiClientImpl(credentials.getOAuthConsumerKeyPair(), credentials.getOAuthAccessTokenPair());
    }

    private void waitSomeTime() {
        try {
            final long timeToWait = 5 + Math.round(Math.floor(Math.random() * 5));
            TimeUnit.SECONDS.sleep(timeToWait);
        } catch (final InterruptedException e) {
            LOGGER.error("Error waiting time", e);
        }
    }

    private void updateActionRequestWithApiResult(final NetworkActionRequest<Twitter> actionRequest,
                                                  final TwitterApiResult result) {
        if (result.equals(TwitterApiResult.SUCCESS)) {
            actionRequest.setSuccess();
        } else {
            if (result.equals(TwitterApiResult.ALREADY_PROCESSED)) {
                actionRequest.setAlreadySucceeded();
            } else {
                if (result.equals(TwitterApiResult.EXCEPTION)) {
                    actionRequest.setForbidden();
                } else {
                    actionRequest.setError();
                }
            }
        }
    }

}
