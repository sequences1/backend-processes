package me.relevante.service.impl;

import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.Network;
import me.relevante.core.RelevanteContext;
import me.relevante.queue.BulkActionMessage;
import me.relevante.request.NetworkActionRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public abstract class AbstractNetworkBulkRequestService<N extends Network, R extends NetworkActionRequest<N>>
        implements NetworkBulkRequestService<N, R> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractNetworkBulkRequestService.class);

    protected final CrudRepository<R, String> actionRequestRepo;
    private final CrudRepository<RelevanteContext, String> relevanteContextRepo;

    public AbstractNetworkBulkRequestService(final CrudRepository<R, String> actionRequestRepo,
                                             final CrudRepository<RelevanteContext, String> relevanteContextRepo) {
        this.relevanteContextRepo = relevanteContextRepo;
        this.actionRequestRepo = actionRequestRepo;
    }

    @Override
    public BulkActionMessage process(final BulkActionMessage bulkActionRequest) {
        final R actionRequest = actionRequestRepo.findOne(bulkActionRequest.getRequestId());
        try {
            if (actionRequest.isIntegrated()) {
                return null;
            }
            execute(actionRequest);
            actionRequest.setIntegrated();
            actionRequestRepo.save(actionRequest);
        } catch (final Exception e) {
            LOGGER.error("Error executing bulk action request ", e);
        } finally {
            final RelevanteContext relevanteContext = relevanteContextRepo.findOne(actionRequest.getRequesterRelevanteId());
            relevanteContext.setLastActionDate(new Date());
            relevanteContextRepo.save(relevanteContext);
        }
        return null;
    }

    protected abstract void execute(final R actionRequest);
}
