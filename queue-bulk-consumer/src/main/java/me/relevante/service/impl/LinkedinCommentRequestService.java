package me.relevante.service.impl;

import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinComment;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.RelevanteContext;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.LinkedinCommentRequest;
import me.relevante.request.LinkedinEngageStatus;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class LinkedinCommentRequestService
        extends AbstractLinkedinEngageBulkRequestService<LinkedinCommentRequest, LinkedinComment>
        implements NetworkBulkRequestService<Linkedin, LinkedinCommentRequest> {

    @Autowired
    public LinkedinCommentRequestService(final CrudRepository<LinkedinCommentRequest, String> commentRequestRepo,
                                         final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                         final CrudRepository<LinkedinComment, String> commentRepo,
                                         final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                         final CrudRepository<LinkedinFullProfile, String> fullProfileRepo,
                                         final RelevanteProfileContextService relevanteProfileContextService) {
        super(commentRequestRepo, relevanteContextRepo, commentRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService);
    }

    @Override
    protected void updateActionEngageStatus(final LinkedinEngageStatus engageStatus,
                                            final ActionEngageStatus actionEngageStatus) {
        engageStatus.setCommentStatus(actionEngageStatus);
    }

    @Override
    public Class<LinkedinCommentRequest> getActionRequestClass() {
        return  LinkedinCommentRequest.class;
    }

    @Override
    protected LinkedinComment createActionFromActionRequest(final LinkedinCommentRequest commentRequest) {
        return new LinkedinComment(commentRequest.getRequesterRelevanteId(), commentRequest.getTargetProfileId(), commentRequest.getTargetPostId(),
                commentRequest.getComment());
    }

}
