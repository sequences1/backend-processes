package me.relevante.service;

import me.relevante.queue.BulkActionMessage;

/**
 * @author Daniel Ibanez
 */
public interface BulkProcessService {
    BulkActionMessage process(BulkActionMessage bulkActionRequest);
}
