package me.relevante.backend;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableRabbit
@EnableFeignClients("me.relevante.api")
@SpringBootApplication
@ComponentScan("me.relevante")
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}