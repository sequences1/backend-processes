package me.relevante.backend.model;

import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.TwitterProfile;
import me.relevante.guesser.Clues;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public class TwitterCluesPopulator implements NetworkCluesPopulator<Twitter> {

    private final CrudRepository<TwitterFullProfile, String> fullProfileRepo;

    @Autowired
    public TwitterCluesPopulator(final CrudRepository<TwitterFullProfile, String> fullProfileRepo) {
        this.fullProfileRepo = fullProfileRepo;
    }

    @Override
    public void populate(final Clues clues,
                         final String networkProfileId) {
        final TwitterFullProfile fullProfile = fullProfileRepo.findOne(networkProfileId);
        final TwitterProfile profile = fullProfile.getProfile();
        if (StringUtils.isBlank(clues.getName()) && StringUtils.isNotBlank(profile.getName())) {
            clues.withName(profile.getName(), "");
        }
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }
}
