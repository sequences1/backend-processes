package me.relevante.backend.model;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.guesser.Clues;

/**
 * @author Daniel Ibanez
 */
public interface NetworkCluesPopulator<N extends Network> extends NetworkEntity<N> {
    void populate(Clues clues, String networkProfileId);
}
