package me.relevante.service;

import me.relevante.queue.ProfileIndexMessage;
import me.relevante.queue.RemoveProfileFromIndexMessage;
import me.relevante.search.SearchProfile;
import me.relevante.search.SearchProfileCreator;
import me.relevante.search.SearchProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileIndexService {

    private final SearchProfileCreator searchProfileCreator;
    private final SearchProfileRepository searchProfileRepository;

    @Autowired
    public ProfileIndexService(final SearchProfileCreator searchProfileCreator,
                               final SearchProfileRepository searchProfileRepository) {
        this.searchProfileCreator = searchProfileCreator;
        this.searchProfileRepository = searchProfileRepository;
    }

    public void process(final ProfileIndexMessage request) {
        final SearchProfile searchProfile = searchProfileCreator.create(request.getUniqueProfileId());
        searchProfileRepository.save(searchProfile);
    }

    public void process(final RemoveProfileFromIndexMessage request) {
        searchProfileRepository.remove(request.getUniqueProfileId());
    }
}
