package me.relevante.service;

import me.relevante.core.Network;
import me.relevante.core.NetworkFullProfileWithPosts;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * @author daniel-ibanez
 */
public abstract class AbstractNetworkProfileMonitoringService<N extends Network, F extends NetworkFullProfileWithPosts<N, ?, ?, F>>
        implements NetworkProfileMonitoringService<N> {

    private final static int MAX_HOURS_SINCE_LAST_PROFILE_DATA_UPDATE = 24 * 7;
    private final static int MAX_HOURS_SINCE_LAST_PROFILE_ACTIVITY_UPDATE = 24;

    protected boolean isProfileDataStale(final F fullProfile) {
        if (fullProfile.getLastUpdatedTimestamp() == null) {
            return true;
        }
        final ZonedDateTime topDateToConsiderProfileDataStale = ZonedDateTime
                .ofInstant(fullProfile.getLastUpdatedTimestamp().toInstant(), ZoneOffset.UTC)
                .plusHours(MAX_HOURS_SINCE_LAST_PROFILE_DATA_UPDATE);
        final ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        return now.isAfter(topDateToConsiderProfileDataStale);
    }

    protected boolean isProfileActivityStale(final F fullProfile) {
        if (fullProfile.getLastActivityUpdatedTimestamp() == null) {
            return true;
        }
        final ZonedDateTime topDateToConsiderProfileActivityStale = ZonedDateTime
                .ofInstant(fullProfile.getLastActivityUpdatedTimestamp().toInstant(), ZoneOffset.UTC)
                .plusHours(MAX_HOURS_SINCE_LAST_PROFILE_ACTIVITY_UPDATE);
        final ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        return now.isAfter(topDateToConsiderProfileActivityStale);
    }
}
