package me.relevante.service;

import me.relevante.backend.persistence.RelevanteAccountRepo;
import me.relevante.backend.persistence.RelevanteContextRepo;
import me.relevante.backend.persistence.UniqueProfileRepo;
import me.relevante.core.Network;
import me.relevante.core.NetworkProfileId;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Search;
import me.relevante.core.UniqueProfile;
import me.relevante.core.WatchlistProfile;
import me.relevante.model.MonitoringComparator;
import me.relevante.util.MapByNetwork;
import me.relevante.util.UserProfileToProfileIdBiAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author daniel-ibanez
 */
@Service
public class ProfileMonitoringService {

    private final static int MAX_RELEVANTE_USERS = 5000;
    private final static int MAX_DAYS_SINCE_LAST_ACCESS_TO_BE_CONSIDERED_ACTIVE = 15;
    private final static int MIN_HOURS_SINCE_LAST_MONITORING = 24;
    private final static Logger logger = LoggerFactory.getLogger(ProfileMonitoringService.class);

    private final UniqueProfileRepo uniqueProfileRepo;
    private final RelevanteAccountRepo relevanteAccountRepo;
    private final RelevanteContextRepo relevanteContextRepo;
    private final Map<String, NetworkProfileMonitoringService> networkProfileMonitoringServiceByNetwork;
    private final UserProfileToProfileIdBiAdapter userProfileAdapter;
    private final MonitoringComparator monitoringComparator;

    @Autowired
    public ProfileMonitoringService(final UniqueProfileRepo uniqueProfileRepo,
                                    final RelevanteAccountRepo relevanteAccountRepo,
                                    final RelevanteContextRepo relevanteContextRepo,
                                    final List<NetworkProfileMonitoringService> networkProfileMonitoringServices,
                                    final UserProfileToProfileIdBiAdapter userProfileAdapter,
                                    final MonitoringComparator monitoringComparator) {
        this.uniqueProfileRepo = uniqueProfileRepo;
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.relevanteContextRepo = relevanteContextRepo;
        this.networkProfileMonitoringServiceByNetwork = MapByNetwork.from(networkProfileMonitoringServices);
        this.userProfileAdapter = userProfileAdapter;
        this.monitoringComparator = monitoringComparator;
    }

    public void monitor() {
        final List<RelevanteContext> relevanteContexts = new ArrayList<>(relevanteContextRepo.findAll(new PageRequest(0, MAX_RELEVANTE_USERS,
                new Sort(Sort.Direction.ASC, "lastMonitoringTimestamp"))).getContent());
        relevanteContexts.sort(monitoringComparator);
        for (final RelevanteContext relevanteContext : relevanteContexts) {
            monitorAccount(relevanteContext);
        }
    }

    private void monitorAccount(final RelevanteContext relevanteContext) {
        final RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteContext.getRelevanteId());
        if (hasNotUsedRelevanteRecently(relevanteAccount) || hasBeenMonitoredTooRecently(relevanteContext)) {
            return;
        }
        if (relevanteAccount.isActive()) {
            monitorWatchlistProfiles(relevanteContext, relevanteAccount);
            monitorSearches(relevanteContext, relevanteAccount);
        }
        updateLastMonitoringTimestampInAccount(relevanteContext);
    }

    private boolean hasNotUsedRelevanteRecently(final RelevanteAccount relevanteAccount) {
        return !hasUsedRelevanteRecently(relevanteAccount);
    }

    private boolean hasUsedRelevanteRecently(final RelevanteAccount relevanteAccount) {
        if (relevanteAccount.getLastAccessTimestamp() == null) {
            return false;
        }
        final ZonedDateTime lastAccessTime = ZonedDateTime.ofInstant(relevanteAccount.getLastAccessTimestamp().toInstant(), ZoneOffset.UTC);
        final ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        final ZonedDateTime oldestAllowedActiveTime = now.minusDays(MAX_DAYS_SINCE_LAST_ACCESS_TO_BE_CONSIDERED_ACTIVE);
        return (lastAccessTime.isAfter(oldestAllowedActiveTime));
    }

    private boolean hasBeenMonitoredTooRecently(final RelevanteContext relevanteContext) {
        if (relevanteContext.getLastMonitoringTimestamp() == null) {
            return false;
        }
        final ZonedDateTime lastMonitoringTimestamp = ZonedDateTime.ofInstant(relevanteContext.getLastMonitoringTimestamp().toInstant(), ZoneOffset.UTC);
        final ZonedDateTime nextAllowedMonitoringTimestamp = lastMonitoringTimestamp.plusHours(MIN_HOURS_SINCE_LAST_MONITORING);
        final ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        return (now.isBefore(nextAllowedMonitoringTimestamp));
    }

    private void monitorSearches(final RelevanteContext relevanteContext,
                                 final RelevanteAccount relevanteAccount) {
        final Collection<Search> searches = relevanteContext.getSearches();
        final List<Search> sortedSearches = new ArrayList<>(searches);
        sortedSearches.sort(monitoringComparator);
        for (final Search search : sortedSearches) {
            monitorSearch(search, relevanteAccount);
        }
    }

    private void monitorWatchlistProfiles(final RelevanteContext relevanteContext,
                                          final RelevanteAccount relevanteAccount) {
        final Collection<WatchlistProfile> watchlistProfiles = relevanteContext.getAllWatchlistsProfiles();
        final List<WatchlistProfile> sortedWatchlistProfiles = new ArrayList<>(watchlistProfiles);
        sortedWatchlistProfiles.sort(monitoringComparator);
        final List<String> profileIds = userProfileAdapter.toProfileIds(sortedWatchlistProfiles);
        for (final String profileId : profileIds) {
            monitorWatchlistProfile(profileId, relevanteAccount);
        }
    }

    private void updateLastMonitoringTimestampInAccount(final RelevanteContext relevanteContext) {
        final RelevanteContext mostRecentRelevanteContext = relevanteContextRepo.findOne(relevanteContext.getRelevanteId());
        mostRecentRelevanteContext.setLastMonitoringTimestamp(Date.from(LocalDateTime.now(ZoneOffset.UTC).toInstant(ZoneOffset.UTC)));
        relevanteContextRepo.save(mostRecentRelevanteContext);
    }

    private void monitorSearch(final Search search,
                               final RelevanteAccount relevanteAccount) {
        for (final NetworkProfileMonitoringService networkProfileMonitoringService : networkProfileMonitoringServiceByNetwork.values()) {
            monitorWatchlistSearchInNetwork(search, relevanteAccount, networkProfileMonitoringService.getNetwork());
        }
        updateLastMonitoringTimestampInSearch(search.getId(), relevanteAccount.getId());
    }

    private void monitorWatchlistSearchInNetwork(final Search search,
                                                 final RelevanteAccount relevanteAccount,
                                                 final Network network) {
        if (relevanteAccount.isNotConnectedToNetwork(network)) {
            return;
        }
        final NetworkProfileMonitoringService networkProfileMonitoringService = networkProfileMonitoringServiceByNetwork.get(network.getName());
        try {
            networkProfileMonitoringService.processSearchTerms(search.getSearchTerms(), relevanteAccount);
        } catch (Exception e) {
            logger.error("Error monitoring watchlist", e);
        }
    }

    private void updateLastMonitoringTimestampInSearch(final String searchId,
                                                       final String relevanteId) {
        final RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        final Search search = relevanteContext.getSearch(searchId);
        search.setLastMonitoringTimestamp(Date.from(LocalDateTime.now(ZoneOffset.UTC).toInstant(ZoneOffset.UTC)));
        relevanteContextRepo.save(relevanteContext);
    }

    private void monitorWatchlistProfile(final String watchlistProfileId,
                                         final RelevanteAccount relevanteAccount) {
        final UniqueProfile uniqueProfile = uniqueProfileRepo.findOne(watchlistProfileId);
        for (final NetworkProfileId networkProfileId : uniqueProfile.getNetworkProfileIds()) {
            if (relevanteAccount.isConnectedToNetwork(networkProfileId.getNetwork())) {
                monitorWatchlistProfileInNetwork(networkProfileId, relevanteAccount);
            }
        }
        updateLastMonitoringTimestampInWatchlistProfile(watchlistProfileId, relevanteAccount.getId());
    }

    private void monitorWatchlistProfileInNetwork(final NetworkProfileId networkProfileId,
                                                  final RelevanteAccount relevanteAccount) {
        final NetworkProfileMonitoringService networkProfileMonitoringService = networkProfileMonitoringServiceByNetwork.get(networkProfileId.getNetwork());
        try {
            networkProfileMonitoringService.processProfile(networkProfileId.getProfileId(), relevanteAccount);
        } catch (Exception e) {
            logger.error("Error monitoring watchlist profile", e);
        }
    }

    private void updateLastMonitoringTimestampInWatchlistProfile(final String profileId,
                                                                 final String relevanteId) {
        final RelevanteContext relevanteContext = relevanteContextRepo.findOne(relevanteId);
        final List<WatchlistProfile> watchlistProfiles = relevanteContext.findWatchlistProfilesByProfileId(profileId);
        final Date lastMonitoringTimestamp = Date.from(LocalDateTime.now(ZoneOffset.UTC).toInstant(ZoneOffset.UTC));
        watchlistProfiles.forEach(watchlistProfile -> watchlistProfile.setLastMonitoringTimestamp(lastMonitoringTimestamp));
        relevanteContextRepo.save(relevanteContext);
    }

}
