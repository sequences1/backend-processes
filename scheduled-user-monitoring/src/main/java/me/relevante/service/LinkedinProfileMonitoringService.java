package me.relevante.service;

import me.relevante.backend.persistence.LinkedinExtractProfileActivityRequestRepo;
import me.relevante.backend.persistence.LinkedinExtractProfileDataRequestRepo;
import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.RelevanteAccount;
import me.relevante.request.LinkedinExtractProfileActivityRequest;
import me.relevante.request.LinkedinExtractProfileDataRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author daniel-ibanez
 */
@Component
public class LinkedinProfileMonitoringService
        extends AbstractNetworkProfileMonitoringService<Linkedin, LinkedinFullProfile>
        implements NetworkProfileMonitoringService<Linkedin> {

    private final CrudRepository<LinkedinFullProfile, String> fullProfileRepo;
    private final LinkedinExtractProfileDataRequestRepo extractProfileDataRequestRepo;
    private final LinkedinExtractProfileActivityRequestRepo extractProfileActivityRequestRepo;

    @Autowired
    public LinkedinProfileMonitoringService(final CrudRepository<LinkedinFullProfile, String> fullProfileRepo,
                                            final LinkedinExtractProfileDataRequestRepo extractProfileDataRequestRepo,
                                            final LinkedinExtractProfileActivityRequestRepo extractProfileActivityRequestRepo) {
        this.fullProfileRepo = fullProfileRepo;
        this.extractProfileDataRequestRepo = extractProfileDataRequestRepo;
        this.extractProfileActivityRequestRepo = extractProfileActivityRequestRepo;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public void processSearchTerms(final List<String> searchTerms,
                                   final RelevanteAccount relevanteAccount) {
    }

    @Override
    public void processProfile(final String networkProfileId,
                               final RelevanteAccount relevanteAccount) {
        final LinkedinFullProfile fullProfile = fullProfileRepo.findOne(networkProfileId);
        if (isProfileActivityStale(fullProfile)) {
            final LinkedinExtractProfileActivityRequest extractProfileActivityRequest =
                    new LinkedinExtractProfileActivityRequest(relevanteAccount.getId(), networkProfileId);
            extractProfileActivityRequestRepo.save(extractProfileActivityRequest);
        }
        if (isProfileDataStale(fullProfile)) {
            final LinkedinExtractProfileDataRequest extractProfileDataRequest =
                    new LinkedinExtractProfileDataRequest(relevanteAccount.getId(), networkProfileId);
            extractProfileDataRequestRepo.save(extractProfileDataRequest);
        }
    }

}
