package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.LinkedinLikeRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinLikeRequestRepo extends NetworkActionRequestRepo<LinkedinLikeRequest> {
}
