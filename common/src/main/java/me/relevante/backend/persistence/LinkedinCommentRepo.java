package me.relevante.backend.persistence;

import me.relevante.core.LinkedinComment;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinCommentRepo extends NetworkPostActionRepo<LinkedinComment> {
}
