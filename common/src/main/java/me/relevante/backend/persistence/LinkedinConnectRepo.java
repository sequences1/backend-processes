package me.relevante.backend.persistence;

import me.relevante.core.LinkedinConnect;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinConnectRepo extends MongoRepository<LinkedinConnect, String> {
    LinkedinConnect findOneByTargetProfileIdAndAuthorId(String targetProfileId, String authorId);
}
