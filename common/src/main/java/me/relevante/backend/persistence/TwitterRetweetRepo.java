package me.relevante.backend.persistence;

import me.relevante.core.TwitterRetweet;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterRetweetRepo extends MongoRepository<TwitterRetweet, String> {
	TwitterRetweet findOneByTargetPostIdAndAuthorId(String tweetId, String authorId);
}
