package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.TwitterFavRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterFavRequestRepo extends NetworkActionRequestRepo<TwitterFavRequest> {
}
