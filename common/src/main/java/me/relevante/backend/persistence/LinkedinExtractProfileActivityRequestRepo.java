package me.relevante.backend.persistence;

import me.relevante.request.LinkedinExtractProfileActivityRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Daniel Ibanez
 */
@Repository
public interface LinkedinExtractProfileActivityRequestRepo extends MongoRepository<LinkedinExtractProfileActivityRequest, String> {
}
