package me.relevante.backend.persistence;

import me.relevante.request.TwitterSearchRelatedDataRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Daniel Ibanez
 */
@Repository
public interface TwitterSearchRelatedDataRequestRepo extends MongoRepository<TwitterSearchRelatedDataRequest, String> {
}
