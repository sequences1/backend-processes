package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.LinkedinConnectRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinConnectRequestRepo extends NetworkActionRequestRepo<LinkedinConnectRequest> {
}
