package me.relevante.backend.persistence;

import me.relevante.request.DeduplicationRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeduplicationRequestRepo extends MongoRepository<DeduplicationRequest, String> {
}
