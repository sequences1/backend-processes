package me.relevante.backend.persistence;

import me.relevante.core.LinkedinGroup;
import me.relevante.persistence.NetworkFindByIdsRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinGroupRepo extends NetworkFindByIdsRepo<LinkedinGroup> {
}
