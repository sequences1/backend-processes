package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.LinkedinCommentRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinCommentRequestRepo extends NetworkActionRequestRepo<LinkedinCommentRequest> {
}
