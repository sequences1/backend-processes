package me.relevante.backend.persistence;

import me.relevante.persistence.BaseTwitterFullProfileRepo;
import org.springframework.stereotype.Repository;

/**
 * @author daniel-ibanez
 */
@Repository
public interface TwitterFullProfileRepo extends BaseTwitterFullProfileRepo {
}
