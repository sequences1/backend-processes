package me.relevante.backend.persistence;

import me.relevante.request.LinkedinExtractFeedProfilesRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinExtractFeedProfilesRequestRepo extends MongoRepository<LinkedinExtractFeedProfilesRequest, String> {
}
