package me.relevante.backend.queue.errorHandler;

import me.relevante.queue.QueueService;
import me.relevante.queue.SendEmailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

import java.io.PrintWriter;
import java.io.StringWriter;

@Component
public class CustomErrorHandler implements ErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomErrorHandler.class);

	private final String from;
	private final String to;
	private final boolean reportEnabled;
    private final QueueService queueService;

    @Autowired
    public CustomErrorHandler(@Value("${spring.rabbitmq.errors.from}") final String from,
                              @Value("${spring.rabbitmq.errors.to}") final String to,
                              @Value("${spring.rabbitmq.errors.enableReport}") final String reportEnabled,
                              final QueueService queueService) {
        this.from = from;
        this.to = to;
        this.reportEnabled = Boolean.valueOf(reportEnabled);
        this.queueService = queueService;
    }

    @Override
	public void handleError(final Throwable t) {
        try {
            final StringWriter errors = new StringWriter();
            t.printStackTrace(new PrintWriter(errors));
            final String stackTrace = errors.toString();
            LOGGER.error("Error", t);
			if (reportEnabled) {
				queueService.sendSendEmailMessage(new SendEmailMessage(from, to, "error", stackTrace));
			}
		} finally {
			throw new AmqpRejectAndDontRequeueException(t);
		}
	}
}